-----------------------------------------------------------------------------
--  S44tree_sprucec
-----------------------------------------------------------------------------
local defs = {}

defs["s44tree_sprucec"] =  {
    name            = "S44tree_sprucec",
    description     = "S44 tree",
    world           = "All Worlds",
    object          = "features/s44tree_sprucec.dae",
    featuredead     = "s44tree_sprucec_destroyed_001",
    footprintX      = 2,
    footprintZ      = 2,
    height          = 38,
    blocking        = true,
    flammable       = true,
    burnable        = true,
    reclaimable     = false,
    autoreclaimable = true,  
    noSelect        = false,
    indestructible  = false,
    damage          = 100,
    energy          = 0,
    upright         = true,
    floating        = false,
    hitdensity      = 5,
    collisionVolumeTest = 1,
    collisionVolumeType = "cylY",
    collisionVolumeScales = {8, 38, 8},
    collisionVolumeOffsets = {0, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "tree", 
        set          = "S44 Trees", 
 
        randomrotate = "true", 
    }, 
}

defs["s44tree_sprucec_destroyed_001"] =  {
    name            = "S44tree_sprucec_destroyed_001",
    description     = "S44 tree",
    world           = "All Worlds",
    object          = "features/s44tree_sprucec_destroyed_001.dae",
    featuredead     = "s44tree_destroyed",
    footprintX      = 2,
    footprintZ      = 2,
    height          = 38,
    blocking        = true,
    flammable       = true,
    burnable        = true,
    reclaimable     = false,
    autoreclaimable = true,  
    noSelect        = false,
    indestructible  = false,
    damage          = 100,
    energy          = 0,
    upright         = true,
    floating        = false,
    hitdensity      = 5,
    collisionVolumeTest = 1,
    collisionVolumeType = "cylY",
    collisionVolumeScales = {8, 38, 8},
    collisionVolumeOffsets = {0, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "tree", 
        set          = "S44 Trees", 
 
        randomrotate = "true", 
    }, 
}

return lowerkeys( defs )
