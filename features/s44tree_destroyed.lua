-----------------------------------------------------------------------------
--  S44tree_destroyed
-----------------------------------------------------------------------------
local defs = {}

defs["s44tree_destroyed"] =  {
    name            = "S44tree_destroyed",
    description     = "S44 tree",
    world           = "All Worlds",
    object          = "features/s44tree_destroyed.dae",
    footprintX      = 1,
    footprintZ      = 1,
    height          = 63,
    blocking        = false,
    flammable       = false,
    burnable        = false,
    reclaimable     = false,
    autoreclaimable = true,  
    noSelect        = false,
    indestructible  = true,
    damage          = 100,
    energy          = 0,
    upright         = true,
    floating        = false,
    hitdensity      = 5,
    collisionVolumeTest = 1,
    collisionVolumeType = "cylY",
    collisionVolumeScales = {2, 2, 2},
    collisionVolumeOffsets = {0, 0, 0},
    customparams = {
        author       = "S44", 
        category     = "tree", 
        set          = "S44 Trees", 
 
        randomrotate = "true", 
    }, 
}

return lowerkeys( defs )
