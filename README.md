Houses features for Spring:1944

PLACE THE ARCHIVE IN YOUR GAMES FOLDER!

Games/mods can add this archive as a dependency and map authors can then use featureplacer in Gundam RTS/Evolution RTS/Other to place features. Mapconv already has support for featureplacer configs.

Adding this archive as a dependency also enables that game/mod to use maps that were previously limited to Spring:1944. This also allows for map sizes to be smaller as there is no longer any need to include the textures with the map.

Additionally, maps may also call this archive as a dependency. Maps can also include the featuredef files if necessary so that reclaim and blocking values can be changed. Basically, the point is that mappers can change the features up however they like with this archive.

Have fun!

Examples of proper dependency usage:

Game calling this archive as dependency:

```
return {
name='Balanced Annihilation V7.31',
description='Moooooo!',
shortname='BA',
version='V7.31',
mutator='Official',
game='Total Annihilation',
shortGame='TA',
modtype=1,
depend= {
-- Number of other content names this one replaces
"S44 Trees 1.0"
},
}
```

Map calling this archive as a dependency:

```
local mapinfo = {
name = "Comet",
mapfile = "maps/Comet Catcher Redux.smf",
depend = {
"S44 Trees 1.0",
},
}

return mapinfo 
```
